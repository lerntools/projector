var express=require('express');
var router=express.Router();
var controller=require('./controller');
var mainController=require('../../../main/server/controller');

//author routes
router.get('/room', controller.checkAuth, controller.getProjector);

//participant routes
router.get('/part/rooms/:ticket', controller.checkTicket, controller.returnTicket);
router.post('/part/rooms/:ticket', controller.checkTicket, controller.sendMessage);

//Websocket um Änderungen an Teilnehmer zu signalisieren
router.ws('/ws/:ticket', controller.initWs);

module.exports=router;
