# projector

Online projector that allows students to transfer photos to the lecturer's browser.

This repository is an optional module for the "lerntools". For installation and configuration, please see the documentation in https://codeberg.org/lerntools/base
